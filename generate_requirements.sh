#!/bin/bash
docker run --rm --env HOME=/home/jovyan --mount type=bind,source="$(pwd)",target=/home/jovyan \
            --platform linux/amd64 --entrypoint bash jupyter/scipy-notebook:2023-04-10 -c \
            "pip install uv; uv pip compile --output-file requirements.txt requirements.in"