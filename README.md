# Local install

This readme guides you through the process of setting up the example pipeline for ellipsometry locally on your working machine. There are two methods available, one is to use a pre-built docker container with all packages installed and the second method is installing the python packages in the local python version. The docker install is easier and recommended.

## Docker

### Setup of docker and docker-compose

If you already have a running docker and docker-compose on your local system you can skip this section.
To setup your local docker on a workstation we recommand using docker desktop, which is a program to provide docker functionality on desktop machines. Please follow their [install instructions](https://docs.docker.com/desktop/#download-and-install).

### Running of ellipsometry examples

Please make sure docker desktop is up and running.
To run the provided examples you just have to execute the command
`docker-compose up`
in the example directory.
This will spin up the docker containers for you and you should be able to access the jupyter hub by clicking on the link at the end of the docker output.

## Bare-metal install

It is recommended to use a python virtualenv for the installation of the packages so you keep it seperated from your other packages as some of the procedures install specific version of packages. We recommend [pyenv](https://github.com/pyenv/pyenv) for python version management and it's plugin [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv) for virtualenv management. [Here](https://janakiev.com/blog/jupyter-virtual-envs/) is a guide on how to setup a virtualenv and how to make it available in jupyter.

First install all required packages by executing

```shell
pip install -r requirements.txt
```

in this directory.

In the next step jupyter and its extensions should be rebuild.

```shell
jupyter lab build
jupyter nbextension enable --py widgetsnbextension
jupyter serverextension enable jupyterlab_h5web
```

Now you should be able to start the example notebooks with jupyter lab. To start the jupyter server just execute the command
`jupyter lab`
in the example directory.
